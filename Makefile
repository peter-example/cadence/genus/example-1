all:
	genus -f synth.tcl

clean:
	rm -fr fv
	rm -fr genus.cmd*
	rm -fr genus.log*
	rm -fr synth.v
