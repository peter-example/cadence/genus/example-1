module WireExample (BpW, Error, Wait, Valid, Clear);
	input Error, Wait, Valid, Clear;
	output BpW;
	wire BpW;
	assign BpW = Error & Wait;
	assign BpW = Valid | Clear;
endmodule

